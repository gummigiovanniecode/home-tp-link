from pyHS100 import SmartPlug, SmartBulb, Discover
from pprint import pformat as format

import socket
import sys
import threading

HOST = ''
PORT = 8888
threads = []
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

try:
    s.bind((HOST, PORT))
except socket.error as msg:
    print ('Bind failed. Error Code : ' + str(msg[0]) + ' Message ' + msg[1])
    sys.exit()

print ('Socket bind complete')

s.listen(10)
print ('Socket now listening')


def clientthread(conn):
    conn.send('Welcome to the server. Type something and hit enter\n')

    while True:

        data = conn.recv(1024)
        reply = 'OK...' + data
        if not data:
            break

        conn.sendall(reply)

    conn.close()


def home():
    plug = SmartPlug("192.168.1.160")

    if plug.state == 'ON':
        plug.turn_off()
    else:
        plug.turn_on()


while 1:
    conn, addr = s.accept()
    thread = threading.Thread(target=home)
    threads.append(thread)
    thread.start()

s.close()