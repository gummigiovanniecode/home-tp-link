package com.igummicodes.giovannie.homelistener;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.widget.Toast;

import com.github.tbouron.shakedetector.library.ShakeDetector;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;

public class servicelistener extends Service {
    private Socket socket;
    private static final int port = 8888;
    private static final String ip = "192.168.1.145";

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        ShakeDetector.create(this, new ShakeDetector.OnShakeListener() {
            @Override
            public void OnShake() {
                /* Toast.makeText(getApplicationContext(), "Device shaken!", Toast.LENGTH_SHORT).show(); */
                new Thread(new ClientThread()).start();

            }
        });

        return Service.START_NOT_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    class ClientThread implements Runnable {

        @Override
        public void run() {

            try {
                InetAddress serverAddress = InetAddress.getByName(ip);
                socket = new Socket(serverAddress, port);

            } catch (UnknownHostException error) {
                Writer writer = new StringWriter();
                error.printStackTrace(new PrintWriter(writer));
                error.printStackTrace();

            } catch (IOException error) {
                Writer writer = new StringWriter();
                error.printStackTrace(new PrintWriter(writer));
                error.printStackTrace();

            }

        }
    }
}