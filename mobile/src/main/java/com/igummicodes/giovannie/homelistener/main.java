package com.igummicodes.giovannie.homelistener;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class main extends AppCompatActivity {
    private Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(com.example.giovannie.homelistener.R.layout.main);

        context = getApplicationContext();
        Button button = (Button) findViewById(com.example.giovannie.homelistener.R.id.button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i= new Intent(context, servicelistener.class);
                i.putExtra("KEY1", "Value to be used by the service");
                context.startService(i);
                Toast.makeText(getApplicationContext(), "service started", Toast.LENGTH_SHORT).show();

            }
        });
    }
}